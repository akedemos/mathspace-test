/**
 *
 * SubTopicCard
 *
 */

import React from 'react'
import { Paper, Avatar, RaisedButton } from 'material-ui'
import tickImage from '../../assets/tick-big.svg'

const cardStyle = {
  maxWidth: 360,
  width: '80%',
  marginLeft: 'auto',
  marginRight: 'auto'
}

const divStyle = {
  color: '#fff',
  margin: '0 0 200px',
  padding: '20% 20px',
  textAlign: 'center',
  background: '#8C88FF',
  position: 'relative',
  zIndex: 1
}

const afterDivStyle = {
  bottom: 0,
  transform: 'skewY(-12deg)',
  transformOrigin: '100%',
  background: 'inherit',
  content: '',
  display: 'block',
  height: '100%',
  left: 0,
  position: 'absolute',
  right: 0,
  zIndex: -1
}

const tickStyle = {
  backgroundColor: '#50D2C2'
}

const buttonDivStyle = {
  height: 100,
  width: '100%',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center'
}

const buttonStyle = {
  borderRadius: 20,
  backgroundColor: '#50D2C2',
  width: '50%',
  textAlign: 'center',
  color: '#FFF'
}

class SubTopicCard extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render () {
    let { subtopic } = this.props
    return (
      <Paper style={cardStyle} rounded>
        <div style={divStyle}>
          <h3>{subtopic.index}. {subtopic.title}</h3>
          {subtopic.completed
            ? <Avatar
              src={tickImage}
              size={30}
              style={tickStyle}
            />
            : null}
          <div style={afterDivStyle}></div>
        </div>
        <div style={buttonDivStyle}>
            <RaisedButton
              style={buttonStyle} buttonStyle={buttonStyle}
            >
              Lets Go
            </RaisedButton>
          </div>
      </Paper>
    )
  }
}

SubTopicCard.propTypes = {
  subtopic: React.PropTypes.object.isRequired
}

export default SubTopicCard
