/**
 *
 * Header
 *
 */

import React from 'react'
// import styled from 'styled-components'
import { AppBar, Avatar } from 'material-ui'

const appBarStyle = {
  backgroundColor: '#6563A4',
  boxShadow: null,
  marginBottom: -1
}

const titleStyle = {
  textAlign: 'center'
}

class Header extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render () {
    let { userImage, title } = this.props
    return (
      <AppBar
        style={appBarStyle}
        title={title}
        titleStyle={titleStyle}
        iconElementRight={<Avatar src={userImage} />}
      />
    )
  }
}

Header.propTypes = {
  userImage: React.PropTypes.string.isRequired,
  title: React.PropTypes.string.isRequired
}

export default Header
