/**
 *
 * BottomTabs
 *
 */

import React from 'react'
import { Tabs, Tab, Avatar } from 'material-ui'

import TickIcon from '../../assets/tick.svg'
import PieChartIcon from '../../assets/piechart.svg'
import LineGraphIcon from '../../assets/linegraph.svg'

const tabsStyle = {
  position: 'absolute',
  bottom: 0,
  width: '100%'
}

const tabStyle = {
  backgroundColor: '#6563A4',
}

const avatarStyle = {
  borderRadius: '0%',
  width: 400
}

const imgStyle = {
  userSelect: 'none',
  color: 'rgba(255, 255, 255, 0.701961)',
  display: 'inline-flex',
  alignItems: 'center',
  justifyContent: 'center',
  fontSize: 24,
  backgroundColor: '#6563A4',
  height: 30,
  width: 30,
  marginBottom: 0
}

const StyledTab = (icon) => (
  //<Avatar style={avatarStyle} backgroundColor='#6563A4' src={icon} size={30} />
  <img src={icon} style={imgStyle}/>
)

class BottomTabs extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render () {
    return (
      <Tabs style={tabsStyle} inkBarStyle={{ backgroundColor: "#FFF" }}>
        <Tab style={tabStyle} icon={StyledTab(TickIcon)} />
        <Tab style={tabStyle} icon={StyledTab(PieChartIcon)} />
        <Tab style={tabStyle} icon={StyledTab(LineGraphIcon)} />
      </Tabs>
    )
  }
}

BottomTabs.propTypes = {}

export default BottomTabs
