/*
 *
 * Selector reducer
 *
 */

import { fromJS } from 'immutable';
import {
  CHANGE_INDEX,
} from './constants';

const initialState = fromJS({});

function selectorReducer(state = initialState, action) {
  console.log(state)
  switch (action.type) {
    case CHANGE_INDEX:
      return state;
    default:
      return state;
  }
}

export default selectorReducer;
