/*
 *
 * Selector
 *
 */

import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { makeSubTopicSelector } from './selectors'
import { selectSelectedSubTopic } from '../App/selectors'

import { Stepper, Step, StepButton, Avatar } from 'material-ui'
import { changeIndex } from './actions'

const avatarStyle = {
  margin: 5,
  borderWidth: 1,
  borderColor: '#FFF',
  borderStyle: 'solid',
  backgroundColor: null
}

const completedStyle = Object.assign({}, avatarStyle, {
  backgroundColor: '#50D2C2'
})


const getAvatar = (completed, index) => (
  <Avatar size={35} style={completed ? completedStyle : avatarStyle}>{index + 1}</Avatar>
)

export class Selector extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render () {
    let { selectedIndex, subtopics, changeIndex } = this.props
    return (
      <Stepper activeStep={selectedIndex} linear={false} style={{backgroundColor: '#6563A4', marginBottom: 15}}>
        {subtopics.map((subtopic, index) => (
          <Step key={index}>
            <StepButton
              icon={getAvatar(subtopic.completed, index)}
              onTouchTap={() => changeIndex(index)}
            />
          </Step>
        ))}
        </Stepper>
    )
  }
}

Selector.propTypes = {

}

const mapStateToProps = createStructuredSelector({
  subtopics: makeSubTopicSelector(),
  selectedIndex: selectSelectedSubTopic()
})

function mapDispatchToProps (dispatch) {
  return {
    changeIndex: (index) => dispatch(changeIndex(index))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Selector)
