import { createSelector } from 'reselect'

/**
 * Direct selector to the selector state domain
 */

const selectTopic = () => (state) => state.get('topic');

/**
 * Other specific selectors
 */

/**
 * Default selector used by Selector
 */

const makeSubTopicSelector = () => createSelector(
  selectTopic(),
  (substate) => substate.toJS().subtopics
);

export default makeSubTopicSelector;
export {
  makeSubTopicSelector
};
