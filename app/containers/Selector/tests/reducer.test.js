
import { fromJS } from 'immutable';
import selectorReducer from '../reducer';

describe('selectorReducer', () => {
  it('returns the initial state', () => {
    expect(selectorReducer(undefined, {})).toEqual(fromJS({}));
  });
});
