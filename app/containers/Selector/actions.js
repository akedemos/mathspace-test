/*
 *
 * Selector actions
 *
 */

import {
  CHANGE_INDEX,
} from './constants';

export function changeIndex(index) {
  return {
    type: CHANGE_INDEX,
    payload: index
  };
}
