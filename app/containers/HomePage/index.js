/*
 *
 * HomePage
 *
 */

import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { selectTopic, selectUser, selectSelectedSubTopic } from '../../containers/App/selectors'

import { Paper } from 'material-ui'
import Header from '../../components/Header'
import SubTopicCard from '../../components/SubTopicCard'
import Selector from '../Selector'
import BottomTabs from '../../components/BottomTabs'

export class HomePage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render () {
    let { user, topic, selectedSubTopic } = this.props
    return (
      <Paper style={{ width: '100%' }} zDepth={0}>
        <Header userImage={user.image} title={topic.title} />
        <Selector />
        <SubTopicCard subtopic={topic.subtopics[ selectedSubTopic ]} />
        <BottomTabs />
      </Paper>
    )
  }
}

HomePage.propTypes = {
  dispatch: PropTypes.func.isRequired,
}

const mapStateToProps = createStructuredSelector({
  topic: selectTopic,
  user: selectUser,
  selectedSubTopic: selectSelectedSubTopic()
})

function mapDispatchToProps (dispatch) {
  return {
    dispatch,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage)
