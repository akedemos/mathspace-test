// makeSelectLocationState expects a plain JS object for the routing state
const makeSelectLocationState = () => {
  let prevRoutingState
  let prevRoutingStateJS

  return (state) => {
    const routingState = state.get('route') // or state.route

    if (!routingState.equals(prevRoutingState)) {
      prevRoutingState = routingState
      prevRoutingStateJS = routingState.toJS()
    }

    return prevRoutingStateJS
  }
}

const selectUser = (state) => state.get('user').toJS()
const selectTopic = (state) => state.get('topic').toJS()
const selectSelectedSubTopic = () => (state) => state.get('selectedSubTopic')

export {
  makeSelectLocationState,
  selectUser,
  selectTopic,
  selectSelectedSubTopic
}
