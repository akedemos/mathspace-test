## Mathspace Test Project

Repo is based off the react-boilerplate project (https://github.com/react-boilerplate/react-boilerplate)

## Get Started

After cloning/extractiing the repo

`
npm install
npm start
`

then open a browser at http://localhost:3000
(Please note that this is running project in development with hot-reloading enabled)

## License

This project is licensed under the MIT license, Copyright (c) 2017 Maximilian
Stoiber. For more information see `LICENSE.md`.
